### To setup your CLI for use with Cloudformation & Terraform

1. If you already have awscli setup, please backup your configurations and keys.
2. We are mandating that you use `aws-vault` as many of the instructions are written assuming the tool is present
If this is a problem, let us know immediately so we can accommodate.

### Setting up AWS Vault
1. Locate the `.aws` folder
2. Copy the contents of the `.aws/config` file to your home directory
3. Replace `AWS_ACCOUNT_ID` found in the file with the one emailed to you

After downloading the aws-vault tool to your commandline, create a profile for payoutsnetwork by issuing the following commands:

```
aws-vault add payoutsnetwork
Enter Access Key Id: %%% <-- From your email
Enter Secret Key: %%% <--- From your email
```

You will then be prompted to create a password to access the credentials. Don't forget it!

### Perform a sanity check

`aws-vault exec interview -- env`

You should see your environment variables plus the STS credentials as output
